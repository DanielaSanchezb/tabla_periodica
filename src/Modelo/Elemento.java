/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 * Clase para el manejo de los datos de un elemento
 * @author CARLOS
 */
public class Elemento implements Comparable {

    private String nombre;
    private String simbolo;
    private int valencia;
    
    /**
     * Constructor vacío para la clase Elemento
     */
    public Elemento() {
    }
    
     /**
     *  Constructor para crear un vendedor con cedula y nombre
     * @param nombre un dato de tipo String que almacena el nombre del elemento.
     * @param simbolo  un dato de tipo String que almacena el simbolo  del elemento.
     * @param valencia  un dato de tipo Entero que almacena los numeros de valencia del elemento.
     */
    public Elemento(String nombre, String simbolo, int valencia) {
        this.nombre = nombre;
        this.simbolo = simbolo;
        this.valencia = valencia;
    }
    
    /**
     * Obtiene el nombre del elemento
     * @return un String con el nombre del elemento
     */
    public String getNombre() {
        return nombre;
    }
    
    /**
     *  Actualiza el nombre del elemento
     * @param nombre un dato de tipo String que representa el nombre del elemento
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Obtiene el simbolo del elemento
     * @return un String con el simbolo del elemento
     */
    public String getSimbolo() {
        return simbolo;
    }
    
    /**
     *  Actualiza el simbolo del elemento
     * @param simbolo un dato de tipo String que representa el simobolo del elemento
     */
    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }

    /**
     * Obtiene el numero de valencia del elemento
     * @return un Entero con el numero de valencia del elemento
     */
    public int getValencia() {
        return valencia;
    }

    /**
     *  Actualiza el numero de valencia del elemento
     * @param valencia un dato de tipo Entero que representa el numero de valencia del elemento
     */
    public void setValencia(int valencia) {
        this.valencia = valencia;
    }

    @Override
    public String toString() {
        return "Elemento{" + "nombre=" + nombre + ", Simbolo=" + simbolo + ", valencia=" + valencia + '}';
    }
    
    public boolean equals(Object other)
    {
     if(other instanceof String) return this.simbolo.equals(other);
     if(other instanceof Elemento) return this.simbolo.equals(((Elemento)other).getSimbolo()) || 
        this.nombre.equals(((Elemento)other).getNombre());
     return false;
    }
    
    
    
    @Override
    public int compareTo(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

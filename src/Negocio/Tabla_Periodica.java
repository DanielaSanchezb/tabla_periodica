/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Elemento;

/**
 *
 * @author Carlos
 */
public class Tabla_Periodica {

    private Elemento tabla_Periodica[];
    private int index;

    public Tabla_Periodica(int numElementos) {
        this.tabla_Periodica = new Elemento[numElementos];
        this.index = 0;
    }

    /**
     * compara si hay dos elementos iguales
     *
     * @param elemento
     * @return un boolean true si ya existe el elemento o false si aun no existe
     */
    private boolean contieneElemento(Elemento elemento) {
        boolean existe = false;
        for (int i = 0; i < tabla_Periodica.length && !existe; i++) {
            Elemento el = this.tabla_Periodica[i];
            if (el != null && elemento.equals(el)) {
                existe = true;
                i = tabla_Periodica.length;
            }
        }
        return existe;
    }
    
    /**
     * 
     * @return el array de elementos de la tabla periodica
     */
    public Elemento[] getTabla_Periodica() {
        return tabla_Periodica;
    }
    /**
     * 
     * @return un int con la cantidad de elementos.
     */
    public int getIndex() {
        return index;
    }

    /**
     * agrega elementos UNICOS a la tabla periodica
     *
     * @param elemento
     * @return un boolean true si el elemento se agrego satisfactoriamente o un
     * false si no lo hizo
     * @throws Exception
     */
    public boolean agregarElemento(Elemento elemento) throws Exception {

        if (elemento == null) {
            throw new Exception("Error, digite el elemento");
        }

        if (elemento.getValencia() < -3 || elemento.getValencia() > 8) {
            throw new Exception("Error, la valencia digitada no puede estar en el rango[-3,8] ");
        }

        if (contieneElemento(elemento)) {
            throw new Exception("El elemento digitado ya existe");
        }

        boolean agrego = false;
        if (!this.contieneElemento(elemento) && this.index < this.tabla_Periodica.length) {
            this.tabla_Periodica[this.index] = elemento;
            this.index++;
            agrego = true;
        }
        return agrego;
    }

    /**
     * Busca un elemento de acuerdo a su simbolo y lo elimina
     *
     * @param simbolo
     * @return el elemento que se elimino
     */
    public Elemento eliminar(String simbolo) {

        Elemento borrado = null;
        for (int i = 0; i < this.index; i++) {
            if (this.tabla_Periodica[i].getSimbolo().equals(simbolo)) {
                borrado = this.tabla_Periodica[i];
                for (int j = i; j < this.index - 1; j++) {
                    this.tabla_Periodica[j] = this.tabla_Periodica[j + 1];
                }
                this.tabla_Periodica[--this.index] = null;
                i = tabla_Periodica.length;
            }
        }
        return borrado;
    }
    /**
     * Busca un elemento por su nombre
     * @param nombre
     * @return el elemento con el nombre que se introdujo como parametro
     */
    public Elemento buscarPorNombre(String nombre) {
        int posicion = -1;
        Elemento elemento;
        for (int i = 0; i < tabla_Periodica.length; i++) {
            if (tabla_Periodica[i] != null && tabla_Periodica[i].getNombre().equals(nombre)) {
                posicion = i;
            }
        }
        elemento = tabla_Periodica[posicion];

        return elemento;
    }
    /**
     * Busca un elemento por su simbolo
     * @param simbolo
     * @return el elemento con el simbolo que se introdujo como parametro
     */
    public Elemento buscarPorSimbolo(String simbolo) {
        int posicion = -1;
        Elemento elemento;
        for (int i = 0; i < tabla_Periodica.length; i++) {
            if (tabla_Periodica[i] != null && tabla_Periodica[i].getSimbolo().equals(simbolo)) {
                posicion = i;
            }
        }
        elemento = tabla_Periodica[posicion];
        return elemento;
    }
    
    /**
     * cuenta los elementos con la valencia pasada por parametro
     * @param valencia
     * @return un entero con la calidad de elementos que tiene la valencia pasada como parametro
     */
    private int valenciaRepetidas(int valencia) {
        int contador = 0;
        for (int i = 0; i < this.index;) {
            if (tabla_Periodica[i] != null) {
                if (tabla_Periodica[i].getValencia() == (valencia)) {
                    contador += 1;
                }
            }
            i++;
        }
        return contador;

    }
    /**
     * Busca un elemento por su valencia
     * @param valencia
     * @return un arreglo de elementos pues varios elementos pueden tener la misma valencia
     */
    public Elemento[] buscarPorValencia(int valencia) {
        int posicion = -1;
        int j = 0;
        int tamanio = valenciaRepetidas(valencia);
        Elemento arrayTemp[] = new Elemento[tamanio];
        for (int i = 0; i < this.index;) {
            if (tabla_Periodica[i] != null && tabla_Periodica[i].getValencia() == (valencia)) {
                posicion = i;
                arrayTemp[j] = tabla_Periodica[posicion];
                j++;
            }
            i++;
        }
        return arrayTemp;
    }

    @Override
    public String toString() {
        String msg = "";
        for (Elemento e : this.tabla_Periodica) {
            if (e != null) {
                msg += e.toString() + "\n";
            }
        }
        return msg;
    }

}

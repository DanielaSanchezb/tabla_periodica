/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Elemento;
import Negocio.Tabla_Periodica;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author juliana
 */
public class Prueba_Tabla_Periodica {

    public static void main(String[] args) throws Exception {
        System.out.println("Por favor digite los datos de la tabla periodica...\n");
        
        Scanner lector = new Scanner(System.in);
        System.out.println("Por favor digite cantidad de elementos que desea para su tabla:");
        int numero = lector.nextInt();

        Tabla_Periodica tabla = new Tabla_Periodica(numero);

        for (int i = 0; i < numero; i++) {
            System.out.println("Por favor digite el nombre del elemento numero " + (i+1) + ":");
            String nombre = lector.next();
            System.out.println("Por favor digite el simobolo del elemento numero " + (i+1) + ":");
            String simbolo = lector.next();
            System.out.println("Por favor digite la valencia del elemento numero " + (i+1) + ":");
            int valencia = lector.nextInt();
            Elemento elemento = new Elemento(nombre, simbolo, valencia);
            tabla.agregarElemento(elemento);
        }
        System.out.println(tabla);
        int opcion = menuTabla();
        switch(opcion){
            case 1: 
                System.out.println("Buscar elemento por simbolo: Por favor digite el simbolo a buscar:");
                String s = lector.next();
                System.out.println("El elemento es: " + tabla.buscarPorSimbolo(s).toString());
                break;
            case 2:
                System.out.println("Buscar elemento por nombre: Por favor digite el nombre a buscar:");
                String n = lector.next();
                System.out.println("El elemento es: " + tabla.buscarPorNombre(n).toString());
                break;
            case 3:
                System.out.println("Buscar elemento por valencia: Por favor digite la valencia a buscar:");
                int v = lector.nextInt();
                Elemento temp[] = tabla.buscarPorValencia(v);
                for(int i=0; i<temp.length; i++)
                System.out.println("El elemento es: " + temp[i].toString());
                break;
            case 4: 
                System.out.println("Eliminar elemento por el simbolo: Por favor digite el simbolo:");
                String e = lector.next();
                System.out.println("El elemento es: " + tabla.eliminar(e).toString());
                break;
            case 5:
                System.out.println("Saliendo... :(");
                break;
            default:
                System.out.println("Numero de operacion no valida");
        }
        
    }
    public static int menuTabla(){
        System.out.println("Operaciones para realizar con su tabla periodica...");
        System.out.println("1. Buscar por simbolo.");
        System.out.println("2. Buscar por nombre.");
        System.out.println("3. Buscar por valencia.");
        System.out.println("4. Eliminar por simbolo.");
        System.out.println("5. Salir.");
        System.out.println("Digite unicamente el numero de la operacion que desee realizar:");
        Scanner lector = new Scanner(System.in);
        int operacion = lector.nextInt();
        return operacion;
    }
    
}

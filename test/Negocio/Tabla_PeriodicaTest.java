/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Elemento;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author juliana
 */
public class Tabla_PeriodicaTest {

    Tabla_Periodica tabla;

    public Tabla_PeriodicaTest() throws Exception {
        tabla = new Tabla_Periodica(5);
        tabla.agregarElemento(new Elemento("S", "s", 1));
        tabla.agregarElemento(new Elemento("D", "d", 2));
        tabla.agregarElemento(new Elemento("F", "f", 3));
        tabla.agregarElemento(new Elemento("G", "g", 4));
        tabla.agregarElemento(new Elemento("H", "h", 1));
    }

    /**
     * Test of agregarElemento method, of class Tabla_Periodica.
     */
    @Test
    public void testAgregarElemento() throws Exception {
        Tabla_Periodica myTabla = new Tabla_Periodica(5);
        myTabla.getTabla_Periodica()[0] = new Elemento("S", "s", 1);
        myTabla.getTabla_Periodica()[1] = new Elemento("D", "d", 2);
        myTabla.getTabla_Periodica()[2] = new Elemento("F", "f", 3);
        myTabla.getTabla_Periodica()[3] = new Elemento("G", "g", 4);
        myTabla.getTabla_Periodica()[4] = new Elemento("H", "h", 1);
        
        assertArrayEquals(myTabla.getTabla_Periodica(), tabla.getTabla_Periodica());
    }

    /**
     * Test of eliminar method, of class Tabla_Periodica.
     */
    @Test
    public void testEliminar(){
        tabla.eliminar("f");
        Tabla_Periodica myTabla = new Tabla_Periodica(5);
        myTabla.getTabla_Periodica()[0] = new Elemento("S", "s", 1);
        myTabla.getTabla_Periodica()[1] = new Elemento("D", "d", 2);
        myTabla.getTabla_Periodica()[2] = new Elemento("G", "g", 4);
        myTabla.getTabla_Periodica()[3] = new Elemento("H", "h", 1);
        assertArrayEquals(myTabla.getTabla_Periodica(), tabla.getTabla_Periodica());
    }

    /**
     * Test of buscarPorNombre method, of class Tabla_Periodica.
     */
    @Test
    public void testBuscarPorNombre() {
        Elemento e = tabla.buscarPorNombre("D");
        Elemento esperado = new Elemento("D", "d", 2);
        assertEquals(esperado, e);
    }

    /**
     * Test of buscarPorSimbolo method, of class Tabla_Periodica.
     */
    @Test
    public void testBuscarPorSimbolo() {
        Elemento e = tabla.buscarPorSimbolo("g");
        Elemento esperado = new Elemento("G", "g", 4);
        assertEquals(esperado, e);
    }

    /**
     * Test of buscarPorValencia method, of class Tabla_Periodica.
     */
    @Test
    public void testBuscarPorValencia() {
        Elemento[] e = tabla.buscarPorValencia(1);
        Elemento [] esperado = new Elemento [2];
        esperado[0] = new Elemento("S", "s", 1);
        esperado[1] = new Elemento("H", "h", 1);
        assertArrayEquals(esperado, e);
    }
}
